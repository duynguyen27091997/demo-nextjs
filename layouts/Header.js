import React, {useContext, useEffect, useState} from 'react';
import ThemeContext, {themes} from "../contexts/ThemeContext";
import Menu from "../components/Menu";

const Header = ({handleChangeTheme}) => {
    const theme = useContext(ThemeContext)
    const [btnTheme,setBtnTheme] = useState('')
    const [openMenu,setOpenMenu] = useState(false)
    useEffect(()=>{
        if (theme){
            if (theme === themes.light){
                setBtnTheme('bx-moon')
            }else{
                setBtnTheme('bx-sun')
            }
        }
    },[theme])

    function toggleMenu() {
        setOpenMenu(!openMenu)
    }

    return (
        
            <header className="l-header" id="header">
                <nav className="nav bd-container">
                    <a href="#" className="nav__logo">Tasty</a>

                    <div className={"nav__menu "+ (openMenu ? 'show-menu' :'')} id="nav-menu">
                        <ul className="nav__list">
                            <Menu toggleMenu={toggleMenu}/>

                            <li><i onClick={handleChangeTheme} className={'bx change-theme '+ btnTheme} id="theme-button"/></li>
                        </ul>
                    </div>

                    <div className="nav__toggle" onClick={toggleMenu} id="nav-toggle">
                        <i className='bx bx-menu'/>
                    </div>
                </nav>
            </header>
       
    );
};

export default Header;