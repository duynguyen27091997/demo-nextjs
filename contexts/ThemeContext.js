import React from "react";

export const themes = {
    dark : 'dark',
    light: 'light'
}

const ThemeContext = React.createContext('light');
export default ThemeContext;