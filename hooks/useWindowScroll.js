import {useEffect} from "react";

export default function useWindowScroll(bodyRef){

    useEffect(()=>{
        /*==================== CHANGE BACKGROUND HEADER ====================*/
        function scrollHeader(){
            const nav = document.getElementById('header')
            // When the scroll is greater than 200 viewport height, add the scroll-header class to the header tag
            if(this.scrollY >= 200) nav.classList.add('scroll-header'); else nav.classList.remove('scroll-header')
        }
        window.addEventListener('scroll', scrollHeader)

        /*==================== SHOW SCROLL TOP ====================*/
        function scrollTop(){
            const scrollTop = document.getElementById('scroll-top');
            // When the scroll is higher than 560 viewport height, add the show-scroll class to the a tag with the scroll-top class
            if(this.scrollY >= 560) scrollTop.classList.add('show-scroll'); else scrollTop.classList.remove('show-scroll')
        }
        window.addEventListener('scroll', scrollTop)

        async function scrollReveal(){
            const ScrollReveal = (await import('../vendors/scrollReveal')).default;
            const sr = ScrollReveal({
                origin: 'top',
                distance: '30px',
                duration: 2000,
                reset: true
            });

            sr.reveal(`.home__data, .home__img,
            .about__data, .about__img,
            .services__content, .menu__content,
            .app__data, .app__img,
            .contact__data, .contact__button,
            .footer__content`, {
                interval: 200
            })
        }

        scrollReveal().then(()=>console.log('scrollReveal is loaded !'))
    },[])

}