import {useEffect, useState} from "react";
import {getActiveSection} from "../helpers/helpers";

export default function useActiveSection(){
    const [active,setActive] = useState()

    useEffect(()=>{
        setActive(getActiveSection())
        window.addEventListener('hashchange',function (){
            setActive(getActiveSection())
        })
        /*==================== SCROLL SECTIONS ACTIVE LINK ====================*/
        // const sections = document.querySelectorAll('section[id]')
        //
        // function scrollActive(){
        //     const scrollY = window.pageYOffset
        //
        //     sections.forEach(current =>{
        //         const sectionHeight = current.offsetHeight
        //         const sectionTop = current.offsetTop - 50,
        //             sectionId = current.getAttribute('id')
        //
        //         if(scrollY > sectionTop && scrollY <= sectionTop + sectionHeight){
        //             if (active !== '#'+sectionId)
        //                 setActive('#'+sectionId)
        //         }
        //     })
        // }
        // window.addEventListener('scroll', scrollActive)
    },[])

    return [ active ];
}