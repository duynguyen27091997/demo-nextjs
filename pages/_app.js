import '../styles/styles.scss'
import {Fragment} from "react";
import Head from "next/head";

function MyApp({Component, pageProps}) {
    return (
        <Fragment>
            <Head>
                <title>Responsive website food</title>
                <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet' />
            </Head>
            <Component {...pageProps} />
        </Fragment>
    )
}

export default MyApp
