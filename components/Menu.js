import React, {Fragment, useEffect, useMemo, useState} from 'react';
import useActiveSection from "../hooks/useActiveSection";

const menuItems = [
    {name : 'Home',link:'#home',active:true},
    {name : 'About',link:'#about',active:false},
    {name : 'Services',link:'#services',active:false},
    {name : 'Menu',link:'#menu',active:false},
    {name : 'Contact us',link:'#contact',active:false},
]
const Menu = ({toggleMenu}) => {
    const [list,setList] = useState(menuItems)

    const [active] = useActiveSection()


    useEffect(()=>{
        if (active){
            setList(list.map(item=>({
                ...item,
                active: item.link === active
            })))
        }
    },[active])

    return (
        <Fragment>
            {
                list.map((item,key) =>(
                    <li className="nav__item" onClick={toggleMenu} key={key}>
                        <a href={item.link} className={"nav__link " + (item.active ? 'active-link' : '' ) }>{item.name}</a>
                    </li>
                ))
            }
        </Fragment>
    );
};

export default Menu;